function [ers,idmin] = AE_src_loc_v4(dgs,t,Material)
% This function minimize the norm(dij-c*tij,2)
% returns to the error of each grid point

% Fengqiao.Zhang, Apr. 19, 2017

cp     = Material.cp; % [m/s]
d      = cp.*t*1000;  % [mm]

[ns,ng]   = size(dgs);
ers       = zeros(1,ng);

for ii = 1: ns-1
    for jj = ii+1:ns
        eri       = sqrt((dgs(ii,:)-dgs(jj,:)-(d(ii)-d(jj))).^2); %[mm]
        ers       = ers+eri;
    end
end

ermin     = min(ers);
idmin     = find(ers==ermin);
idmin     = idmin(randi([1 length(idmin)]));
end
% [idx,idy,idz] = ind2sub(sz,idmin);
% 
% S0.x      = gx(idx);
% S0.y      = gy(idy);
% S0.z      = gz(idz);
% S0.ers    = ers;


% ers       = zeros(length(gx),length(gy),length(gz));
% n         = numel(ers);
% sz        = size(ers);
% [X,Y,Z]   = ind2sub(sz,1:n);
% X         = reshape(X,sz);
% Y         = reshape(Y,sz);
% Z         = reshape(Z,sz);
% 
% for ii = 1: length(xs)-1
%     for jj = ii+1:length(xs)
%         d1        = sqrt((xs(ii)-gx(X)).^2+(ys(ii)-gy(Y)).^2+(zs(ii)-gz(Z)).^2);
%         d2        = sqrt((xs(jj)-gx(X)).^2+(ys(jj)-gy(Y)).^2+(zs(jj)-gz(Z)).^2);
%         eri       = (d1-d2-(d(ii)-d(jj))).^2;
%         ers       = ers+eri;
%     end
% end
% 
% ermin     = min(min(min(ers)));
% idmin     = find(ers==ermin,1);
% [idx,idy,idz] = ind2sub(sz,idmin);
% 
% S0.x      = gx(idx);
% S0.y      = gy(idy);
% S0.z      = gz(idz);
% S0.ers    = ers;



