# Fengqiao Zhang PhD project

This space includes the relevant Matlab codes used for the PhD thesis of Fengqiao Zhang. All copyrights reserved.

## Getting started
The folder 'script' stores the files to read data and calculate the probability density field.

The file 'realtime_AE' can be used for real time monitoring, the needed inputs are commented in the script.

The other files are for post processing, where 'pre_read_AE' is to read the raw data, 'post_proc_AE' is to derive important parameters and perform source localizaiton, 'sort_AE' is to sort the data and calculate the probability density field.

The folder 'lib' is the library of the needed functions in these scripts.

