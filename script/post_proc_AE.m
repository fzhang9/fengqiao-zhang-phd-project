% This script using grid to find the minimal Error function, considering
% the crack width and crack angle
% Fengqiao.Zhang,  Nov.5, 2018

clc;
close all;
clf;
clear all;
set(0,'DefaultAxesFontName','Times New Roman'); set(0,'DefaultTextFontName','Times New Roman');
set(0,'DefaultAxesFontSize',14); set(0,'DefaultTextFontSize',14); set(0,'defaultlinelinewidth',2);
addpath('C:\Fengqiao_Office\matlab\lib');

%%
save_key           = 0;   % 1,save; 2, unsave

%% Input : Arrival time, sensor position
% arrival time
DIR_data  = 'C:\Fengqiao_Office\result\H853\';

load([DIR_data 'AEraw.mat']);
load([DIR_data 'lvdt.mat']);
%% combine AE data
wv.ht   = AE(1).waveid;
wv.pp   = AEf(1).pp;
wv.cf   = AEf(1).cf;
wv.pf   = AEf(1).pf;
% time, force, deflection from LVDT
isnt        = find(isnan(n(:,3)));
isnf        = find(isnan(n(:,5)));
isnd        = find(isnan(n(:,25)));
time        = n(:,3);
ldd         = n(:,5);
def         = n(:,25);
for ii = isnt'
    time(ii)   = str2num(r{3+ii,3});
end
for ii = isnf'
    ldd(ii)    = str2num(r{3+ii,5});
end
for ii = isnd'
    def(ii)    = str2num(r{3+ii,25});
end
%% shift AEp time with Lvdt TIME
AEp1 = AE;
AEp  = AE;
figure(1);clf;hold on;plot(AEp1.time,-AEp1.par1/0.6*100);plot(time,-ldd)
disp('select point from AE')
[tAE,~] = ginput(1);
disp('select point from LVDT')
[tLVDT,~] = ginput(1);
t0    = tLVDT-tAE;
AEp.time   = AEp1.time+t0;
plot(AEp.time,-AEp.par1/0.6*100);
disp('select points from LVDT')
[pti,pld] = ginput;

pti(1) = time(1);
pld(1) = -ldd(1);
%% plot geometry in 2D
Geometry.Lb  = 3; % [m]
Geometry.Db  = 0.3;
Geometry.Hb  = 1.2;
Geometry.sen_pos  = [AE.sen_pos(:,1) [zeros(9,1);0.15*ones(5,1)] AE.sen_pos(:,2)];

Lb    = Geometry.Lb;
Db    = Geometry.Db;
Hb    = Geometry.Hb;

rec0  = Geometry.sen_pos;
Geometry.sen_pos(:,1)  = rec0(:,1)+(Lb-rec0(11,1)); % inportant transform=================
rec   = Geometry.sen_pos;

figure(2);clf;
hold on;
plot( [-Lb/6 Lb],[0 0],'k-');
plot( [-Lb/6 Lb],[Hb Hb],'k-');
plot(rec(:,1),rec(:,3),'ok');
text(rec(:,1),rec(:,3),num2str((1:size(rec,1))'));
axis image;box on;
% xlim([-0.5 2.5])
% set(gca,'Xdir','reverse');
xlabel('x [m]');
ylabel('z [m]')

Sensor.x    = rec(:,1); % [m]
Sensor.y    = rec(:,2); % [m]
Sensor.z    = rec(:,3);
 
% wave speed
Material.cp    = 4000; % [m/s]

%% pick up hits in certain loading steps
figure(2);
clf;hold on;plot(time,-ldd);
xlabel('time [s]');
ylabel('load [kN]')
% [pti, pld] = ginput;
% pti=[70.5645161290313;473.790322580644;1219.75806451613;1723.79032258064;2449.59677419355;3074.59677419355;3760.08064516129;4425.40322580645;5090.72580645161;6139.11290322581;6481.85483870968;6804.43548387097;7086.69354838710;7530.24193548387;8114.91935483871;8961.69354838710;9687.50000000000;10372.9838709677;11098.7903225806;11784.2741935484;12550.4032258065;12772.1774193548;13074.5967741935;13739.9193548387;14102.8225806452;14566.5322580645;15292.3387096774;16562.5000000000;18276.2096774194;19485.8870967742;20695.5645161290;21965.7258064516;23437.5000000000;24002.0161290323;24385.0806451613;24868.9516129032;25272.1774193548;25655.2419354839;25997.9838709677;26582.6612903226;27086.6935483871;27691.5322580645;28114.9193548387];
% pld=[3.77564856488522;92.8360084122133;240.421747587786;487.246173450382;16.4985571145035;494.879918580153;13.9539754045800;499.969082000000;13.9539754045800;723.892272473282;779.873070091603;838.398449419847;894.379247038168;932.547972687023;983.439606885496;19.0431388244274;988.528770305343;16.4985571145035;993.617933725191;11.4093936946561;1041.96498621374;1087.76745699237;1118.30243751145;1187.00614367939;1235.35319616794;1248.07610471756;1319.32439259542;21.5877205343509;1314.23522917557;16.4985571145035;1339.68104627481;24.1323022442744;1388.02809876336;1433.83056954199;1487.26678545038;1530.52467451908;1581.41630871756;1632.30794291603;1680.65499540458;1723.91288447328;1769.71535525191;1823.15157116031;108.103498671756];
plot(pti,pld,'x');

pt      = zeros(length(pti)-1,2);
ld      = zeros(length(pld)-1,2);
for ii = 1:length(pti)-1
    pt(ii,:) = [pti(ii) pti(ii+1)];
    ld(ii,:) = [pld(ii) pld(ii+1)];
end
%% Pick source
thr     = 50; % dB
pht     = struct;
lv      = [0 150 0 150 0 200 0 200 0 250 0 200 0 150 0 500 0];% [0 150 0 150 0 200 0 200 0 250 0 200 0 150 0 250 270 300 312 325 350 375 400 450 500 0];
for ii = 1:size(pt,1)
    findht           = find(AEp.time>pt(ii,1) & AEp.time<pt(ii,2) & AEp.amp>thr & AEp.ch<=size(rec,1));
    pht(ii).ld1      = lv(ii);
    pht(ii).ld2      = lv(ii+1);
    if lv(ii)<=lv(ii+1)
        pht(ii).lcon     = 1;
    else
        pht(ii).lcon     = -1;
    end
    pht(ii).ht       = findht;
    pht(ii).thr      = thr;
end
%% 
dt     = norm([max(rec(:,1))-min(rec(:,1)) max(rec(:,3))-min(rec(:,3))])/Material.cp;  % [s]
for ii = 1:length(pht)
    chp    = AEp.ch(pht(ii).ht);
    timep  = AEp.time(pht(ii).ht);
    indgr  = find(timep(2:end)-timep(1:end-1)>dt)+1;
    if any(indgr)
        indgr1  = [1 indgr];
        cnt     = 0;
        for jj = 1:length(indgr)
            ind   = indgr1(jj):(indgr(jj)-1);
            ch    = chp(ind);
            if length(ch)>2 && length(ch) == length(unique(ch))
                id    = find((timep(ind)-timep(ind(1)))*Material.cp<1);
                if length(id)>2
                    cnt  = cnt + 1;
                    pht(ii).grh(cnt).ht    = pht(ii).ht(ind(id));
                end
            end
        end
    end
end
%% Grid
gd                 = 5;    % [mm] % grid size
et                 = 50;  %[mm] extended grid search zone
gx                 = (min(rec(:,1))*1000-et-gd/2:gd:max(rec(:,1))*1000+et+gd/2); 
gz                 = (min(rec(:,3))*1000-et-gd/2:gd:max(rec(:,3))*1000+et+gd/2); 

gp                 = zeros(1,length(gx)*length(gz)); % number of grid points
n                  = numel(gp);
sz                 = [length(gx),length(gz)];
[GX,GZ]            = ind2sub(sz,1:n);

Grid.x             = gx(GX); % [mm]
Grid.z             = gz(GZ);

%% localization
% calculate the distance between each grid point and each sensor
dgs                = sqrt((Sensor.x*1000-gx(GX)).^2+(Sensor.z*1000-gz(GZ)).^2); % [mm]
for ii = 1:length(pht)
    disp(['part' num2str(ii) 'begins calculation...']);
    tstart         = tic;
    % localization
    x  = zeros(1,length(pht(ii).grh));
    z  = zeros(1,length(pht(ii).grh));
    er = zeros(1,length(pht(ii).grh));
    ttsrc  = length(pht(ii).grh);
    cnt = 0;
    keep = [];
    for jj = 1:length(pht(ii).grh)
        htind  = pht(ii).grh(jj).ht;
        t      = AEp.time(htind)'; % [s]
        ch     = AEp.ch(htind);
        % Localization
        [ers,idmin]        = AE_src_loc_v4(dgs(ch,:),t,Material);
        x(jj)              = Grid.x(idmin)/1000;
        z(jj)              = Grid.z(idmin)/1000;
        er(jj)             = min(ers)/1000;
        pht(ii).grh(jj).loc   = [x(jj) z(jj)];
        pht(ii).grh(jj).err   = er(jj);
        sloc                  = rec(ch,:);
        if any(diff(sloc(:,3)))
            k          = convhull(sloc(:,1)',sloc(:,3)');
            in         = inpolygon(x(jj),z(jj),sloc(k,1)',sloc(k,3)');
            if in
                cnt    = cnt +1;
                keep(cnt)  = jj;
            end
        end
    end
    pht(ii).grh_kp       = keep;
    
    tend  = toc(tstart);
    disp([ 'finishes; Time:' num2str(tend) 's']);
end

if save_key ==1
    save('C:\Fengqiao_Office\result\H853A\loc','AEp','pht','wv','Geometry','Grid','Material','Sensor','time','ldd','t0','pti','pld','def');
end
return;
%% plot

    
%% save
% if save_key==1
%     save('D:\fzhang9\2018-2019\I23\I23_monitoring\result\AEmonitor\loc','pht','Cell','A','cursor_info');
% end



