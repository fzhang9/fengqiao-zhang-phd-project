% This script check the possible failure criteria using AE
% Fengqiao.Zhang,  Apr.6, 2020

clc;
close all;
clf;
clear all;
set(0,'DefaultAxesFontName','Times New Roman'); set(0,'DefaultTextFontName','Times New Roman');
set(0,'DefaultAxesFontSize',14); set(0,'DefaultTextFontSize',14); set(0,'defaultlinelinewidth',2);
addpath('C:\Fengqiao_Office\matlab\lib');

%%
save_key           = 0;   % 1,save; 2, unsave

%% load data
% arrival time
DIR_data  = 'C:\Fengqiao_Office\result\H853A\';

load([DIR_data 'loc.mat']);
load([DIR_data 'lvdt.mat']); 

%% plot geometry in 2D
Lb    = Geometry.Lb;
Db    = Geometry.Db;
Hb    = Geometry.Hb;
rec   = Geometry.sen_pos;
figure(2);clf;
hold on;
plot( [-Lb/6 Lb],[0 0],'k-');
plot( [-Lb/6 Lb],[Hb Hb],'k-');
plot(rec(:,1),rec(:,end),'ok');
text(rec(:,1),rec(:,end),num2str((1:size(rec,1))'));
axis image;box on;
xlabel('x [m]');
ylabel('z [m]')

%% kept sources
rs     = 29/2/1000; % [m] radius of sensor, as source reference distance
for ii = 1:length(pht)
    cnt   = 0;
    kp2   = [];
    for jj =1:length(pht(ii).grh)
        loc   = pht(ii).grh(jj).loc;
        ht    = pht(ii).grh(jj).ht;
        ch    = AEp.ch(ht);
        minx  = min(rec(ch,1));
        maxx  = max(rec(ch,1));
        minz  = min(rec(ch,end));
        maxz  = max(rec(ch,end));
        if loc(1)>minx+rs&&loc(1)<maxx-rs&&loc(2)>minz+rs&&loc(2)<maxz-rs
            cnt  = cnt + 1;
            kp2(cnt)   = jj;
        end
    end
    pht(ii).grh_kp2   = kp2;
end

%% organise results
% source localization results
LOC   = struct;
lv    = zeros(length(pht),2);
lcon  = zeros(length(pht),1);
for ii = 1:length(pht)
    for jj = 1:length(pht(ii).grh_kp)
        ind  = pht(ii).grh_kp(jj);
        LOC(ii).loc(jj,:)   = pht(ii).grh(ind).loc;
        LOC(ii).ht{jj}      = pht(ii).grh(ind).ht;
    end
    lv(ii,:)          = [pht(ii).ld1 pht(ii).ld2];
    lcon(ii)          = pht(ii).lcon;
end

% estimate the event energy
alpha  = 20; % [dB/m]
for ii = 1:length(LOC)
    for jj = 1:length(LOC(ii).ht)
        ht  = LOC(ii).ht{jj};
        amp = AEp.amp(ht);
        htrep  = ht(find(max(amp),1));
        LOC(ii).htrep(jj) = htrep;

        enerr   = AEp.ener(htrep); % [pVs]
        ch      = AEp.ch(htrep);
        dd      = norm(LOC(ii).loc(jj,:)-rec(ch,[1 end]));
        deltaA  = alpha*dd+db(dd/rs);
        fct     = db2mag(deltaA);
        srcE    = enerr*fct;
        LOC(ii).srcE(jj)  = srcE;
    end
end

% filter out signal from SA
for ii = 1:length(LOC)
    if LOC(ii).htrep
        LOC(ii).SAid  = [];
        SAid   = find(AEp.amp(LOC(ii).htrep)>90);
        if SAid
            ind    = find(SAid(2:end-1)-SAid(1:end-2)==1 & SAid(2:end-1)-SAid(3:end)==-1);
            if ind
                SAid   = SAid([ind ind(end)+1 ind(end)+2]);
                LOC(ii).SAid  = SAid;
            end
        end
    end
end
for ii = 1:length(LOC)
    LOC(ii).AEid    = setdiff(1:length(LOC(ii).htrep),LOC(ii).SAid);
end

for ii = 1:length(LOC)
    LOC(ii).ld1   = pht(ii).ld1;
    LOC(ii).ld2   = pht(ii).ld2;
    LOC(ii).lcon  = pht(ii).lcon;
end

%
LOC(26)= LOC(25);
cnt = 0;
for ii = 15:24
    cnt = cnt + 1;
    LOC(25).loc = [LOC(25).loc;LOC(ii).loc];
    LOC(25).ht = [LOC(25).ht LOC(ii).ht];
    LOC(25).htrep = [LOC(25).htrep LOC(ii).htrep];
    LOC(25).srcE = [LOC(25).srcE LOC(ii).srcE];
    LOC(25).SAid = [LOC(25).SAid LOC(ii).SAid];
    LOC(25).AEid = [LOC(25).AEid LOC(ii).AEid];
    LOC(25).AEstep(cnt).AEid=LOC(ii).AEid;
    LOC(25).AEstep(cnt).ld1=LOC(ii).ld1;
    LOC(25).AEstep(cnt).ld2=LOC(ii).ld2;
end
LOC(25).ld1=0;
LOC(25).ld2=500;
LOC(25).lcon=1;
LOC(25).pti_small=pti(15:25)';
LOC(25).pld_small=pld(15:25)';
LOC(15:24) = [];
pti      = pti([1:15 25 26]);
pld      = pld([1:15 25 26]);

%%
figure(5);clf;hold on;
plot(time,-ldd);
ylabel('load [kN]')
xlabel('time [s]')
box on;
plot(pti,pld,'x')
%% plot AE results
celld  = 250/1000;%[m]
% AE during loading
for ii = 1:length(LOC)
    if LOC(ii).lcon == 1
        if LOC(ii).htrep
            loc = LOC(ii).loc(LOC(ii).AEid,:);
            figure(ii*10);clf;hold on;
            subplot(2,1,1);hold on;
            plot(loc(:,1),loc(:,2),'.r');
            plot( [-Lb/6 Lb],[0 0],'k-');
            plot( [-Lb/6 Lb],[Hb Hb],'k-');
            plot(rec(:,1),rec(:,end),'ok');
            text(rec(:,1)-0.05,rec(:,end),num2str((1:size(rec,1))'));
            axis image;box on;
            %     xlim([-0.5 2.5])
            xlabel('x [m]');
            ylabel('z [m]')
            title(['load step ' num2str(max([pht(ii).ld1 pht(ii).ld2])) 'kN (' num2str(fix(pld(ii))) '-' num2str(fix(pld(ii+1))) 'kN)'])
            
            subplot(2,1,2);hold on;
            [values, centers] = hist3(loc,'Ctrs',{min(rec(:,1))+celld/2:celld:max(rec(:,1))+celld/2 min(rec(:,end))+celld/2:celld:max(rec(:,end))+celld/2});
            imagesc(centers{:}, values.')
            colorbar('west')
            colormap(flipud(gray));
            plot(rec(:,1),rec(:,end),'ok');
            text(rec(:,1)-0.05,rec(:,end),num2str((1:size(rec,1))'));
            plot( [-Lb/6 Lb],[0 0],'k-');
            plot( [-Lb/6 Lb],[Hb Hb],'k-');
            axis image;box on;
            %     xlim([-0.5 2.5])
            xlabel('x [m]');
            ylabel('z [m]');
        end
    end
end
% AE during unloading
for ii = 1:length(LOC)
    if LOC(ii).lcon == -1
        if LOC(ii).htrep
            loc = LOC(ii).loc(LOC(ii).AEid,:);
            figure(ii*10);clf;hold on;
            subplot(2,1,1);hold on;
            plot(loc(:,1),loc(:,2),'.r');
            plot( [-Lb/6 Lb],[0 0],'k-');
            plot( [-Lb/6 Lb],[Hb Hb],'k-');
            plot(rec(:,1),rec(:,end),'ok');
            text(rec(:,1)-0.05,rec(:,end),num2str((1:size(rec,1))'));
            axis image;box on;
            %     xlim([-0.5 2.5])
            xlabel('x [m]');
            ylabel('z [m]')
            title(['load step ' num2str(max([pht(ii).ld1 pht(ii).ld2])) 'kN (' num2str(fix(pld(ii))) '-' num2str(fix(pld(ii+1))) 'kN)'])
            
            subplot(2,1,2);hold on;
            [values, centers] = hist3(loc,'Ctrs',{min(rec(:,1))+celld/2:celld:max(rec(:,1))+celld/2 min(rec(:,end))+celld/2:celld:max(rec(:,end))+celld/2});
            imagesc(centers{:}, values.')
            colorbar('west')
            colormap(flipud(gray));
            plot(rec(:,1),rec(:,end),'ok');
            text(rec(:,1)-0.05,rec(:,end),num2str((1:size(rec,1))'));
            plot( [-Lb/6 Lb],[0 0],'k-');
            plot( [-Lb/6 Lb],[Hb Hb],'k-');
            axis image;box on;
            %     xlim([-0.5 2.5])
            xlabel('x [m]');
            ylabel('z [m]')
        end
    end
end


%% detailed look at small load steps before failure
pti150  = zeros(1,length(smallstep150)+2);
pld150  = zeros(1,length(smallstep150)+2);
pti150(1) = pti(1);
pld150(1) = pld(1);
for ii = 1:length(smallstep150)
    pti150(ii+1)  = smallstep150(ii).Position(1);
    pld150(ii+1)  = smallstep150(ii).Position(2);
end
pti150(end)     = pti(2);
pld150(end)     = pld(2);
[pti150,id]     = sort(pti150);
pld150          = pld150(id);
pti_small{1}    = pti150;
pld_small{1}    = pld150;
timeAE      = AEp.time(LOC(1).htrep);
lvs{1}       =  [0 50 100 150];
for ii = 1:length(pti150)-1
    AEid   = find(timeAE>=pti150(ii)&timeAE<pti150(ii+1));
    LOC(1).AEstep(ii).AEid   = setdiff(AEid,LOC(1).SAid);
end

pti200  = zeros(1,length(smallstep200)+2);
pld200  = zeros(1,length(smallstep200)+2);
pti200(1) = pti(5);
pld200(1) = pld(5);
for ii = 1:length(smallstep200)
    pti200(ii+1)  = smallstep200(ii).Position(1);
    pld200(ii+1)  = smallstep200(ii).Position(2);
end
pti200(end)     = pti(6);
pld200(end)     = pld(6);
[pti200,id]     = sort(pti200);
pld200          = pld200(id);
pti_small{2}    = pti200;
pld_small{2}    = pld200;
timeAE      = AEp.time(LOC(5).htrep);
lvs{2}       =  [0 150 175 185 200];
for ii = 1:length(pti200)-1
    AEid   = find(timeAE>=pti200(ii)&timeAE<pti200(ii+1));
    LOC(5).AEstep(ii).AEid   = setdiff(AEid,LOC(5).SAid);
end

pti250  = zeros(1,length(smallstep250)+2);
pld250  = zeros(1,length(smallstep250)+2);
pti250(1) = pti(9);
pld250(1) = pld(9);
for ii = 1:length(smallstep250)
    pti250(ii+1)  = smallstep250(ii).Position(1);
    pld250(ii+1)  = smallstep250(ii).Position(2);
end
pti250(end)     = pti(10);
pld250(end)     = pld(10);
[pti250,id]     = sort(pti250);
pld250          = pld250(id);
pti_small{3}    = pti250;
pld_small{3}    = pld250;
timeAE   = AEp.time(LOC(9).htrep);
lvs{3}        =  [0 200 220 240 250];
for ii = 1:length(pti250)-1
    AEid   = find(timeAE>=pti250(ii)&timeAE<pti250(ii+1));
    LOC(9).AEstep(ii).AEid = setdiff(AEid,LOC(9).SAid);
end


cnt   = 0;
smallstep  = [1 5 9];%[1 5 9];

for ii = 1%smallstep
    cnt   = cnt + 1;
    LOC(ii).pti_small   = pti_small{cnt};
    LOC(ii).pld_small   = pld_small{cnt};
    for jj = 1:length(LOC(ii).AEstep)
        LOC(ii).AEstep(jj).ld1 = lvs{cnt}(jj);
        LOC(ii).AEstep(jj).ld2 = lvs{cnt}(jj+1);
    end
end
%
ldst   = 10; % [kN]
for ii = setdiff(1:length(LOC),[smallstep 15])
    tid    = find(time>=pti(ii)&time<pti(ii+1));
    if LOC(ii).lcon==1
        ldrg   = 10:ldst:(LOC(ii).ld2-10);
    else
        ldrg = (LOC(ii).ld1-10):-ldst:10;
    end
    ptii   = zeros(1,length(ldrg));
    pldi   = zeros(1,length(ldrg));
    ptii(1)   = pti(ii);
    ptii(end) = pti(ii+1);
    pldi(1)   = pld(ii);
    pldi(end) = pld(ii+1);
    for jj = 2:length(ldrg)-1
        [~,ldid]  = min(abs(-ldd(tid)-ldrg(jj)));
        id  = tid(ldid(end));
        ptii(jj) = time(id);
        pldi(jj) = -ldd(id);
    end
    LOC(ii).pti_small = ptii;
    LOC(ii).pld_small = pldi;
    timeAE     = AEp.time(LOC(ii).htrep);
    for jj = 1:length(ptii)-1
        AEid   = find(timeAE>=ptii(jj)&timeAE<ptii(jj+1));
        LOC(ii).AEstep(jj).AEid   = setdiff(AEid,LOC(ii).SAid);
        LOC(ii).AEstep(jj).ld1    = ldrg(jj);
        LOC(ii).AEstep(jj).ld2    = ldrg(jj+1);
    end
    %     end
end
%% use old file
for ii = [5 9 15]
    if ~isempty(alldata(6).LOC(ii).AEstep)
        LOC(ii).pti_small   = alldata(6).LOC(ii).pti_small;
        LOC(ii).pld_small   = alldata(6).LOC(ii).pld_small;
        
        timeAE              = AEp.time(LOC(ii).htrep);
        ptii                = LOC(ii).pti_small;
        pldi                = LOC(ii).pld_small;
                  
        for jj = 1:length(alldata(6).LOC(ii).AEstep)
            AEid            = find(timeAE>=ptii(jj)&timeAE<ptii(jj+1));
            LOC(ii).AEstep(jj).AEid  = setdiff(AEid,LOC(ii).SAid);
            LOC(ii).AEstep(jj).ld1   = alldata(6).LOC(ii).AEstep(jj).ld1;
            LOC(ii).AEstep(jj).ld2   = alldata(6).LOC(ii).AEstep(jj).ld2;
        end
    end
end
figure(1);clf;hold on;
plot(time,-ldd);
ylabel('load [kN]')
xlabel('time [s]')
box on;
plot(pti,pld,'x')
for ii = 1:length(LOC)
    if LOC(ii).pti_small
        plot(LOC(ii).pti_small,LOC(ii).pld_small,'+')
    end
end
% repeat time
rpt   = [1 1 2 2 1 1 2 2 1 1 3 3 3 3 1 1];
for ii = 1:length(LOC)
    LOC(ii).rpt   = rpt(ii);
end
%%
% deflection from LVDT  
% def1         = n(:,25);
def2         = n(:,26);
% isnd1        = find(isnan(def1));
isnd2        = find(isnan(def2));
% if isnd1
%     for ii = isnd1'
%         def1(ii)  = str2num(s{1+ii,25});
%     end
% end
def   = def2;
% indwr = find(abs(def1(2:end)-def1(1:end-1))>3)+1;
% def(indwr(1):indwr(2)) = def1(indwr(1)-1);
% if isnd2
%     for ii = isnd2'
%         def2(ii)  = str2num(s{1+ii,26});
%     end
% end
figure(2);clf;hold on;
plot(time,def);
% plot(time,def2);
xlabel('time [s]')
ylabel('deflection [mm]')

for ii = 1:length(pti)
    [~,ind(ii)] = min(abs(time-pti(ii)));
end
selst = [1 5 9 15];%1:6:(length(ind)-1);
defst = zeros(1,length(selst));
ldst  = zeros(1,length(selst));
area  = zeros(1,length(selst));
cnt  = 0;
for ii = selst
    cnt   = cnt + 1;
    figure(10+cnt);clf;hold on;
    selp  = ind(ii):ind(ii+2);
    plot(def(selp),-ldd(selp));
    xlabel('deflection [mm]')
    ylabel('load [kN]')
    box on;
    area(cnt)  = polyarea(def([selp selp(1)]),-ldd([selp selp(1)]));
    defst(cnt) = max(def(selp))-min(def(selp));
    ldst(cnt)  = LOC(ii).ld2;
end
AEeng  = zeros(1,length(selst));
cnt = 0;
for ii = selst
    cnt  = cnt + 1;
    AEeng(cnt)  = sum(LOC(ii).srcE(LOC(ii).AEid));
end
figure(3);clf;hold on;
yyaxis left;plot(ldst,area./defst);ylabel('F-U/\delta');
yyaxis right;plot(ldst,AEeng./defst);ylabel('W/\delta')
xlabel('load level');
box on;
figure(4);clf;hold on;
plot(ldst,(area-AEeng*1e-5)./defst);
xlabel('load level [kN]')
ylabel('(F-U-W)/\delta [J/mm]');
xlabel('load level');
box on;    

%%





if save_key == 1
    save('C:\Fengqiao_Office\result\H853A\loc_sort','AEp','LOC','wv','Geometry','Grid','Material','Sensor','time','ldd','t0','pti','pld','def');
end
  
return;
%% plot
step  = 13;
for ii = 1:length(LOC(step).AEsstep)
    AEsstep  = LOC(step).AEsstep{ii};
    AEsstep  = setdiff(AEsstep,LOC(step).SAid);
    if AEsstep
        loc = LOC(step).loc(AEsstep,:);
        figure(step*100+ii);clf;hold on;
        subplot(2,1,1);hold on;
        plot(loc(:,1),loc(:,2),'.r');
        plot( [-Lb/6 Lb],[0 0],'k-');
        plot( [-Lb/6 Lb],[Hb Hb],'k-');
        plot(rec(:,1),rec(:,3),'ok');
        text(rec(:,1)-0.05,rec(:,3),num2str((1:size(rec,1))'));
        axis image;box on;
        %     xlim([-0.5 2.5])
        set(gca,'Xdir','reverse');
        xlabel('x [m]');
        ylabel('z [m]')
        title([ num2str(fix(LOC(step).lv(ii))) '-' num2str(fix(LOC(step).lv(ii+1))) 'kN'])
        
        subplot(2,1,2);hold on;
        [values, centers] = hist3(loc,'Ctrs',{min(rec(:,1))+celld/2:celld:max(rec(:,1))+celld/2 min(rec(:,3))+celld/2:celld:max(rec(:,3))+celld/2});
        imagesc(centers{:}, values.')
        colorbar('west')
        colormap(flipud(gray));
        plot(rec(:,1),rec(:,3),'ok');
        text(rec(:,1)-0.05,rec(:,3),num2str((1:size(rec,1))'));
        plot( [-Lb/6 Lb],[0 0],'k-');
        plot( [-Lb/6 Lb],[Hb Hb],'k-');
        axis image;box on;
        %     xlim([-0.5 2.5])
        set(gca,'Xdir','reverse');
        xlabel('x [m]');
        ylabel('z [m]');      
    end
end
%%
% for xci = 1:length(centers{1})
%     xc  = centers{1}(xci);
%     for zci = 1:length(centers{2})
%         zc  = centers{2}(zci);
%         srcincell    = find(LOC(ii).loc(:,1)>xc-celld/2&LOC(ii).loc(:,1)<=xc+celld/2&LOC(ii).loc(:,2)>zc-celld/2&LOC(ii).loc(:,2)<=zc+celld/2);
%         if srcincell
%             srcE(xci,zci)  = sum(LOC(ii).srcE(srcincell));
%         else
%             srcE(xci,zci) = 0;
%         end
%     end
% end
% 
% subplot(3,1,3);hold on;
% imagesc(centers{:}, srcE.')
% colorbar('west')
% colormap(flipud(gray));
% plot(rec(:,1),rec(:,3),'ok');
% text(rec(:,1)-0.05,rec(:,3),num2str((1:size(rec,1))'));
% plot( [-Lb/6 Lb],[0 0],'k-');
% plot( [-Lb/6 Lb],[Hb Hb],'k-');
% axis image;box on;
% %     xlim([-0.5 2.5])
% set(gca,'Xdir','reverse');
% xlabel('x [m]');
% ylabel('z [m]');
        

%% save
% if save_key==1
%     save('D:\fzhang9\2018-2019\I23\I23_monitoring\result\AEmonitor\loc','pht','Cell','A','cursor_info');
% end



