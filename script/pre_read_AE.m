% This script read AE monitor data
% Fengqiao.Zhang,  Nov.5, 2018

clc;
close all;
clf;
clearvars;
set(0,'DefaultAxesFontName','Times New Roman'); set(0,'DefaultTextFontName','Times New Roman');
set(0,'DefaultAxesFontSize',14); set(0,'DefaultTextFontSize',14); set(0,'defaultlinelinewidth',2);
addpath('D:\fzhang9\Matlab\lib');
%%
save_key           = 0;   % 1,save; 2, unsave

%% Input : Arrival time, sensor position
DIR_data  = 'C:\Fengqiao_Office\Data\H853\AEdata\';
fname      = dir([DIR_data '*.txt']);
% ti         = zeros(1,length(fname));
for ii = 1:length(fname)
    disp(['file ' num2str(ii) ' starts reading...']);
    [di,ti,ht,time,par1,ch,rise,coun,ener,durt,amp,Afreq,RMS,ASL,Pcnts,thr,Rfreq,Ifreq,strth,abseng,waveid] = read_AE_para_v3p( [DIR_data fname(ii).name] );
    AE(ii).di    = di;
    AE(ii).ti    = ti;
    AE(ii).ht    = ht;
    AE(ii).time  = time;
    AE(ii).par1  = par1;
    AE(ii).ch    = ch; 
    AE(ii).rise  = rise;
    AE(ii).coun  = coun;
    AE(ii).ener  = ener;
    AE(ii).durt  = durt;
    AE(ii).amp   = amp;
    AE(ii).Afrq = Afreq;
    AE(ii).RMS   = RMS;
    AE(ii).ASL   = ASL;
    AE(ii).Pcnts = Pcnts;
    AE(ii).thr   = thr;
    AE(ii).Rfrq = Rfreq;
    AE(ii).Ifrq = Ifreq;
    AE(ii).strth = strth;
    AE(ii).abseng= abseng;
    AE(ii).waveid=waveid;
    disp('complete');
end

DIR_data   = 'C:\Fengqiao_Office\Data\H853\AEdata\flt\';
fname      = dir([DIR_data '*.txt']);
for ii = 1:length(fname)
    disp(['file ' num2str(ii) ' starts reading...']);
    [di,ti,time,par1,ch,~,~,~,~,~,~,~,~,~,~,~,~,~,~,pp,cf,pf] = read_AE_para_v3p_f( [DIR_data fname(ii).name] );
    AEf(ii).di    = di;
    AEf(ii).ti    = ti;
    AEf(ii).time  = time;
    AEf(ii).par1  = par1;
    AEf(ii).ch    = ch; 
    AEf(ii).pp    = pp;
    AEf(ii).cf    = cf;
    AEf(ii).pf    = pf;
    disp('complete');
end


if save_key
    save('C:\Fengqiao_Office\result\H853\AEraw','AE','AEf');
end
return;

%% pre-read lvdt
filename1   = 'D:\fzhang9\data\H853\H853A.xls';
sheet       = 1;

[n1,r1,s1]     = xlsread(filename1,sheet);
isnt1        = find(isnan(n1(:,3)));
isnf1        = find(isnan(n1(:,5)));
time1        = n1(:,3);
force1       = n1(:,5);

filename2   = 'D:\fzhang9\data\H853\H853B.xls';

[n2,r2,s2]     = xlsread(filename2,sheet);
isnt2        = find(isnan(n2(:,3)));
isnf2        = find(isnan(n2(:,5)));
time2        = n2(:,3);
force2       = n2(:,5);


% for ii = isnt'
%     time(ii)  = str2num(r{3+ii,3});
% end
% for ii = isnf'
%     force(ii)  = str2num(r{3+ii,5});
% end
r = [r1;r2];
n = [n1;n2];
s = [s1;s2];
time   = [time1;time1(end)+time2];
ldd    = [force1;force2];
figure(1);clf;hold on;
plot(time,-ldd,'-');
xlabel('time [s]');
ylabel('Force [kN]');
box on;

if save_key
    save('D:\fzhang9\Results\H853\lvdt','n','s');
end
