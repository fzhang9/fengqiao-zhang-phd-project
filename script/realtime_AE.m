% This script read and process AE monitor data in real time, for running
% speed, the grid size was increased
% Fengqiao.Zhang,  Feb.14, 2021

clc;
close all;
clf;
clearvars;
set(0,'DefaultAxesFontName','Times New Roman'); set(0,'DefaultTextFontName','Times New Roman');
set(0,'DefaultAxesFontSize',14); set(0,'DefaultTextFontSize',14); set(0,'defaultlinelinewidth',2);
addpath('C:\Fengqiao_Office\matlab\lib');

% directory of the folder
DIR_data   = 'U:\PhDFengqiao\2021-2022\Mistras\testNS\data\';
DIR_save   = 'U:\PhDFengqiao\2021-2022\Mistras\testNS\save\';
DIR_output = 'U:\PhDFengqiao\2021-2022\Mistras\testNS\output\';

save_key           = 0;   % 1,save; others, unsave

%% Input: Geometry, material, sensor location, grid, nonlocal averaging setup, and strut-tie
Lb  = 4.5; % [m] half span in longitudinal direction
Wb  = 0.3; % [m] half span in width direction
Hb  = 1.2; % [m] height
d   = 1.158;
rho = 0.57/100;
ne  = 200/30; % Es/Ec
% sensor location and numbering, to be checked!!
ld_pos   = [Lb Wb/2 Hb];
rec      = [4.5-[0.25:0.75:2.5 0.25:0.5:2.75 0.5:0.5:2.5];zeros(1,10) 0.15*ones(1,5);1.112*ones(1,4) 0.612*ones(1,6) zeros(1,5)]';

Geometry.Lb   = Lb;
Geometry.Db   = Wb;
Geometry.Hb   = Hb;
Geometry.sen_pos = rec;
Geometry.ld_pos  = ld_pos;

Sensor.x      = rec(:,1); % [m]
Sensor.y      = rec(:,2);
Sensor.z      = rec(:,3);
Sensor.type   = 'R6I';

Material.cp   = 4000; %[m/s]

gd            = 10;  % [mm] % grid size, was 5 mm
et            = 10;  %[mm] extended grid search zone
gx            = (min(rec(:,1))*1000-et-gd/2:gd:max(rec(:,1))*1000+et+gd/2); %[mm]
gz            = (min(rec(:,3))*1000-et-gd/2:gd:max(rec(:,3))*1000+et+gd/2); 
gp            = zeros(1,length(gx)*length(gz)); % number of grid points
n             = numel(gp);
sz            = [length(gx),length(gz)];
[GX,GZ]       = ind2sub(sz,1:n);

Grid.x        = gx(GX); % [mm]
Grid.z        = gz(GZ);

mu = [0 0];
sg = 55/1000; %[m]
Sigma = [sg 0 ; 0 sg].^2;
% nonlocal averaging, image grid
igd        = 20/1000; %[m] grid spacing
%     image grid selection, with limited space
iGrid      = struct;
%     image grid
igx        = min(rec(:,1)):igd:max(rec(:,1)); %[m]
igz        = min(rec(:,3)):igd:max(rec(:,3));
igp        = zeros(1,length(igx)*length(igz)); % number of grid points
n          = numel(igp);
isz        = [length(igx),length(igz)];
[iGX,iGZ] = ind2sub(isz,1:n);

Nonlocal.mu    = mu;
Nonlocal.Sigma = Sigma;

iGrid.x    = igx(iGX); % [m]
iGrid.z    = igz(iGZ);
iGrid.sz   = isz;

% strut and tie
hcef      = 2.5*(Hb-d);  % [m] effective concrete tension height around steel bar
dzstrut   = Hb-hcef;
dxstrut   = Lb-(sqrt(hcef^2-Hb*hcef+Lb.^2/4)+Lb./2)/2;
alpha     = atan(dzstrut./dxstrut);  % angle of compression struts
width     = hcef./cos(alpha);  % [m] widht of compression struts

dx        = width/2*sin(alpha);
dz        = width/2*cos(alpha);
edge      = [-dx dx Lb Lb-2*dx;hcef 0 Hb-hcef Hb]';

% crack length and major crack spacing
scr    = (1+rho*ne-sqrt(2*rho*ne+(rho*ne).^2))*d;%[m] ultimate crack height
lcr    = scr/1.28; %[m] crack spacing

zone(1).info = 'tie';
zone(1).x    = [Lb-lcr Lb-lcr Lb+lcr Lb+lcr Lb-lcr]';
zone(1).z    = [hcef 0 0 hcef hcef]';
zone(1).angle= 0;
zone(1).cl   = 'g';

zone(2).info = 'strut';
zone(2).x    = edge([1:end 1],1);
zone(2).z    = edge([1:end 1],2);
zone(2).angle= alpha;
zone(2).cl   = 'r';
    
% igrid in each zone
for ii = 1:length(zone)
    x   = zone(ii).x;
    z   = zone(ii).z;
    in  = inpolygon(iGrid.x,iGrid.z,x',z');
    id  = find(in);
    zone(ii).igrid = id;%(ind);
end

% plot
xt   = 0.2;
zt   = 1.1;
figure(1);clf;hold on;
plot( [0 Lb],[0 0],'k-');
plot( [0 Lb],[Hb Hb],'k-');
plot(rec(:,1),rec(:,3),'ok');
for ii = 1:length(zone)
    plot(zone(ii).x,zone(ii).z,['-' zone(ii).cl]);
end
axis image;box on;
xlabel('x [m]');
ylabel('z [m]')
hText1 = text(xt, zt, '');


figure(2);clf;hold on;
plot( [0 Lb],[0 0],'k-');
plot( [0 Lb],[Hb Hb],'k-');
plot(rec(:,1),rec(:,3),'ok');
for ii = 1:length(zone)
    plot(zone(ii).x,zone(ii).z,['-' zone(ii).cl]);
end
axis image;box on;
xlabel('x [m]');
ylabel('z [m]')
hText2 = text(xt,zt,'');

maxAEd     = zeros(1,2);
figure(3);hold on;
for jj = 1:length(zone)
    plot(0,maxAEd(1,jj),['o' zone(jj).cl],'DisplayName',zone(jj).info);
end
legend('Location','westoutside')
xlabel('file No.');
ylabel('max cumulative probabilistic AE density [/m^2]')

%% start!
% event definition time
dt         = norm([max(rec(:,1))-min(rec(:,1)) max(rec(:,3))-min(rec(:,3))])/Material.cp;  % [s]
% calculate the distance between each grid point and each sensor
dgs        = sqrt((Sensor.x*1000-gx(GX)).^2+(Sensor.z*1000-gz(GZ)).^2); % [mm]
fileti     = 0;
probsum    = 0;
while true
    % find all txt files and sort according to time
    fname     = dir([DIR_data '*.txt']);
    [~,idx]   = sort([fname.datenum]);
    fname     = fname(idx);
    if length(fname)&& fname(end).datenum>fileti
        idnewfile  = find([fname.datenum]>fileti);
        fileti     = fname(end).datenum;  % update latest file time
        
        for ii=idnewfile
            disp(['file ' num2str(fname(ii).name) ' data reading...']);
            tstart         = tic;
            % copy file to the folder:'save'
            copyfile([DIR_data fname(ii).name],DIR_save)
            disp('data copied.')
            % read new files
%             [di,ti,ht,time,par1,ch,rise,coun,ener,durt,amp,pp,cf,pf] = read_AE_para_v1_f( [DIR_save fname(ii).name] );
            [di,ti,ht,time,par1,par2,ch,rise,coun,ener,durt,amp,Afreq,RMS,ASL,Pcnts,thr,Rfreq,Ifreq,strth,abseng,waveid] = read_AE_para_v3( [DIR_save fname(ii).name] );
            AE(ii).di    = di;
            AE(ii).ti    = ti;
            AE(ii).ht    = ht;
            AE(ii).time  = time; % [s]
            AE(ii).par1  = par1;
            AE(ii).ch    = ch;
            AE(ii).rise  = rise;
            AE(ii).coun  = coun;
            AE(ii).ener  = ener;
            AE(ii).durt  = durt;
            AE(ii).amp   = amp;
%             AE(ii).pp    = pp;
%             AE(ii).cf    = cf;
%             AE(ii).pf    = pf;
            disp('data read.');
            
            % pick source
            thr          = 50; % dB
            findht       = find(AE(ii).amp>thr & AE(ii).ch<=size(rec,1));
            AE(ii).thr   = thr;
            
            % pick sources based on filtered hits
            chp    = AE(ii).ch(findht);
            timep  = AE(ii).time(findht);
            indgr  = find(timep(2:end)-timep(1:end-1)>dt)+1; % id in 'findht'
            rep    = [];
            if any(indgr)
                indgr1  = [1 indgr]; % id in 'findht'
                cnt     = 0;
                for jj = 1:length(indgr)
                    ind   = indgr1(jj):(indgr(jj)-1); % id in 'findht'
                    chs   = chp(ind);
                    if length(chs)>2 && length(chs) == length(unique(chs))
                        id    = find((timep(ind)-timep(ind(1)))*Material.cp<1); % id in 'ind'
                        if length(id)>2
                            cnt  = cnt + 1;
                            AE(ii).eventht{cnt}   = findht(ind(id));
                            rep(cnt)              = findht(ind(id(1)));
                        end
%                         sloc                  = rec(chs(id),[1 3]);
%                         [k,v]                 = boundary(sloc,0);
%                         if v>0
%                             cnt  = cnt + 1;
%                             AE(ii).eventht{cnt}    = findht(ind(id));
%                         end
                    end
                end
            end
            % remove events from SA
            SAid   = find(AE(ii).amp(rep)>88);
            if SAid
                ind    = find(SAid(2:end-1)-SAid(1:end-2)==1 & SAid(2:end-1)-SAid(3:end)==-1);
                if ind
                    SAid   = SAid([ind ind(end)+1 ind(end)+2]);
                end
            end
            AE(ii).eventht(SAid)=[];
            % source localization
            disp('start localization...');
            x  = zeros(1,length(AE(ii).eventht));
            z  = zeros(1,length(AE(ii).eventht));
            er = zeros(1,length(AE(ii).eventht));
            cnt1 = 0;
            kp   = [];
            x    = [];z = [];locall=[];
            for jj = 1:length(AE(ii).eventht)
                htind  = AE(ii).eventht{jj};
                t      = AE(ii).time(htind)'; % [s]
                chj    = AE(ii).ch(htind);
                [ers,idmin]        = AE_src_loc_v4(dgs(chj,:),t,Material);
                x(jj)              = Grid.x(idmin)/1000; 
                z(jj)              = Grid.z(idmin)/1000;
                locall(jj,:)       = [x(jj) z(jj)];
                sloc               = rec(chj,:);
                minx  = min(sloc(:,1));
                maxx  = max(sloc(:,1));
                minz  = min(sloc(:,end));
                maxz  = max(sloc(:,end));
                if x(jj)>minx&&x(jj)<maxx&&z(jj)>minz&&z(jj)<maxz
                    cnt1  = cnt1 + 1;
                    kp(cnt1)   = jj;
                end
            end
            AE(ii).eventloc         = locall;
            AE(ii).event_filt       = kp;
            disp('source localization finishes.')
            if any(kp)
                loc    = AE(ii).eventloc(kp,:);
                % nonlocal averaging
                disp('start nonlocal averaging...')
                % calculate density for each source
                % calculate the relevant position of each image grid point and each located
                % event
                prob   = zeros(size(loc,1),length(iGrid.x));
                for jj = 1:size(loc,1)
                    rloc  = [iGrid.x'-loc(jj,1) iGrid.z'-loc(jj,2)];
                    % calculate the probability of AE event at each grid point
                    pb    = mvnpdf(rloc,mu,Sigma);
                    prob(jj,:)  = pb';
                end
                AE(ii).prob  = prob;
                disp('nonlocal averaging finishes')
                % calculate max AE density in tie and strut
                for jj=1:length(AE)
                    probsum   = probsum + sum(prob);
                end
                
                for jj= 1:length(zone)
                    id    = zone(jj).igrid;
                    maxAEd(ii+1,jj) = max(probsum(id));
                end
                
                % plot
                figure(1);hold on;
                p1 = plot(loc(:,1),loc(:,2),'.r');
                uistack(p1,'bottom')
                delete(hText1);
                hText1  = text(xt,zt,['until ' fname(ii).name]);
                
                figure(2);hold on;
                value  = reshape(probsum,iGrid.sz);
                p2 = imagesc(iGrid.x,iGrid.z,value');
                uistack(p2,'bottom')
                colormap(flip(gray));
                c = colorbar;
                c.Label.String = 'AE density';
                plot( [0 Lb],[0 0],'k-');
                plot( [0 Lb],[Hb Hb],'k-');
                plot(rec(:,1),rec(:,3),'ok');
                for jj = 1:length(zone)
                    plot(zone(jj).x,zone(jj).z,['-' zone(jj).cl]);
                end
                axis image;box on;
                xlabel('x [m]');
                ylabel('z [m]')
                delete(hText2);
                hText2   = text(xt,zt,['until ' fname(ii).name]);
                
                figure(3);hold on;
                for jj = 1:length(zone)
                    plot([ii-1 ii],[maxAEd(ii,jj) maxAEd(ii+1,jj)],['-' zone(jj).cl],'HandleVisibility','off');
                    plot(ii,maxAEd(ii+1,jj),['o' zone(jj).cl],'HandleVisibility','off');
                end
            end
            tend  = toc(tstart);
            disp([ 'calculation finishes; Time:' num2str(tend) 's']);
        end
    else
        disp('waiting for the new file...')
        pause(10);
    end
end
%%
if save_key
    save([DIR_output 'result'],'AE','Geometry','Sensor','Material','Grid','iGrid');
end

